#include <iostream>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main(){
	
	int fd;
	DatosMemCompartida* pdatos;
	char * proyeccion;

	fd=open("/tmp/datosBot.txt",O_RDWR);
	if (fd==-1){
		perror("Error en la apertura del fichero");
		exit(-1);
	}
	
	proyeccion=(char*)mmap(NULL,sizeof(*(pdatos)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);

	close(fd);

	pdatos=(DatosMemCompartida*) proyeccion;

	while(1){
		usleep(25000);
		float posR1, posR2;
		posR1=((pdatos->raqueta1.y2+pdatos->raqueta1.y1)/2);
		posR2=((pdatos->raqueta2.y2+pdatos->raqueta2.y1)/2);
		
		if(posR1 < pdatos->esfera.centro.y) pdatos->accion1=1;
		else    if(posR1 > pdatos->esfera.centro.y) pdatos->accion1=-1;
			else pdatos->accion1=0;

		if(posR2 < pdatos->esfera.centro.y) pdatos->accion2=1;
		else    if(posR2 > pdatos->esfera.centro.y) pdatos->accion2=-1;
			else pdatos->accion2=0;
	}
	
	munmap(proyeccion, sizeof(*(pdatos)));
}
