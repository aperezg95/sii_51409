#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <iostream>

using namespace std;

int main(int argc, char * argv[])
{
	int fd;
	int nbits;
	char cadena[200];
	unsigned estado=1;
	
	
	if(mkfifo("/tmp/logger",0777)==-1)
	{
		perror("Error en la creación del pipe");
		exit(1);
	}

	fd=open("/tmp/logger", O_RDWR);

	if (fd==-1){
		perror("Error en la apertura del pipe");
		exit(-1);
	}
	
	while (estado)
	{
		nbits=read(fd,cadena,sizeof(cadena));
		if ((nbits==34)||(nbits==29))
		printf("%s \n", cadena);
		if (cadena[0]=='¡') estado=0;
	}
	
	close(fd);
	unlink("/tmp/logger");
	
	return 0;
}
